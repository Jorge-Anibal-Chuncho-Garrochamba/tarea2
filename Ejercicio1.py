# Capitulo 3
# Ejercicio 1
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec"

# Ejercicio 6: Reescribe el programa del cálculo del salario para darle al empleado 1.5 veces la tarifa horaria para todas las horas trabajadas que excedan de 40.


Horas = float(input("Introduzca el numero de horas "))
Tarifa = float(input("Introduzca la tarifa "))

if Horas >= 40:
    Tarifa = Horas * (Tarifa * 1.5)
    print("Su salario es",(Tarifa))
else:
    if Horas > 1:
        Tarifa = Horas * Tarifa
        print("Su salario es", (Tarifa))