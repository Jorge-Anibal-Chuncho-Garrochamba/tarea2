# Capitulo 3
# Ejercicio 2
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec"

# Ejercicio 7: Reescribe el programadel salario usando try y except, de modo que el programa sea capaz de gestionar entradas no numéricas con elegancia, mostrando un mensaje y saliendo del programa.A continuación se muestran dos ejecuciones del programa

try:
    Horas = int(input("Introduzca el numero de horas "))
    Tarifa = int(input("Introduzca la tarifa "))

except ValueError:
    print("Error, por favor introduzca un número")
else:
     if Horas > 40:
        Tarifa = Horas * (Tarifa * 1.5)
        print("Su salario es",(Tarifa))
     else:
         if Horas > 1:
            Tarifa = Horas * Tarifa
            print("Su salario es", (Tarifa))